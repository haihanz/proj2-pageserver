from flask import Flask,render_template, abort

app = Flask(__name__)

@app.route("/")
def hello():
    return "UOCIS docker demo!"

@app.errorhandler(404)
def error_404(e):
    return render_template('404.html'),404

@app.errorhandler(403)
def error_403(e):
    return render_template('403.html'),403


@app.route("/<username>")
def show_username(username):
    if username.startswith("..") or username.startswith("/") or username.startswith("~"):
        abort(403)
    else:
        if username[-4:] == 'html':
            try:
                return render_template(username)
            except:
                abort(404)
        else:
            abort(403)

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
